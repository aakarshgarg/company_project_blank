package com.example.demo.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="Emp_table" , catalog="emp_database")
public class EmpDao {
   
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;  

	@Column  
	private String Emp_name;  

	@Column  
	private int Emp_age;  
	
	@Column  
	private String Emp_email;  
	
	@Column  
	private int Emp_salary;  
	
	@Column  
	private String Emp_department;
	

	public EmpDao() {
		super();
	}
	public EmpDao(int id, String emp_name, int emp_age, String emp_email, int emp_salary, String emp_department) {
		super();
		this.id = id;
		Emp_name = emp_name;
		Emp_age = emp_age;
		Emp_email = emp_email;
		Emp_salary = emp_salary;
		Emp_department = emp_department;
	}
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmp_name() {
		return Emp_name;
	}

	public void setEmp_name(String emp_name) {
		Emp_name = emp_name;
	}

	public int getEmp_age() {
		return Emp_age;
	}

	public void setEmp_age(int emp_age) {
		Emp_age = emp_age;
	}

	public String getEmp_email() {
		return Emp_email;
	}

	public void setEmp_email(String emp_email) {
		Emp_email = emp_email;
	}

	public int getEmp_salary() {
		return Emp_salary;
	}

	public void setEmp_salary(int emp_salary) {
		Emp_salary = emp_salary;
	}

	public String getEmp_department() {
		return Emp_department;
	}

	public void setEmp_department(String emp_department) {
		Emp_department = emp_department;
	}


	@Override
	public String toString() {
		return "EmpDao [id=" + id + ", Emp_name=" + Emp_name + ", Emp_age=" + Emp_age + ", Emp_email=" + Emp_email
				+ ", Emp_salary=" + Emp_salary + ", Emp_department=" + Emp_department + "]";
	}


	
	
	
	

}

