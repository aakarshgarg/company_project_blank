package com.example.demo.Services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.example.demo.dao.EmpDao;
import com.example.demo.repository.EmpRepository;

@Component
public class EmpServicesImpl implements EmpServices{

	@Autowired
	EmpRepository repository;

	public List<EmpDao> getAllEmp() {
		List<EmpDao> empDao = new ArrayList<EmpDao>();
		repository.findAll().forEach(Emp1 -> empDao.add(Emp1));
		return empDao;

	}

	public EmpDao getEmpById(int id) {
		return repository.findById(id).get();
	}

	@Override
	public String saveOrUpdate(EmpDao empDao) {
		System.out.println(empDao.toString());
		return repository.save(empDao).toString();
	}

	public void delete(int id) {
		repository.deleteById(id);
	}

	public void update(EmpDao empdao, int id) {
		repository.save(empdao);
	}


}
