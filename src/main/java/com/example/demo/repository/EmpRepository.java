package com.example.demo.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;


import com.example.demo.dao.EmpDao;

@Component
public interface EmpRepository extends CrudRepository <EmpDao , Integer> {


}