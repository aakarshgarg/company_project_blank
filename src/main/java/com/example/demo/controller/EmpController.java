package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Services.EmpServicesImpl;
import com.example.demo.dao.EmpDao;

@RestController
public class EmpController {

	@Autowired
	EmpServicesImpl empServices;

	public List<EmpDao> getAllEmp() {
		return empServices.getAllEmp();
	}

	@GetMapping(value = "/EmpDao")
	private EmpDao getEmp(int id) {
		EmpDao x = empServices.getEmpById(id);
		return x;
//		System.out.println("ID: "+x.getId());
//		System.out.println("Name: "+x.getEmp_name());
//		System.out.println("Email: "+x.getEmp_email());
//		System.out.println("Sal: "+x.getEmp_salary());
//		System.out.println("Department: "+x.getEmp_department());

	}

//		@GetMapping("/EmpDao/{id}")  
//		private EmpDao getBooks(@PathVariable("id") int id)   
	// {
	// return new EmpDao();
	// return empService.getEmpById(id);
//		}  

	@DeleteMapping("/EmpDao/{id}")
	private void deleteBook(@PathVariable("id") int id) {

		empServices.delete(id);
	}

	@PostMapping(value = "/Emp", consumes = MediaType.APPLICATION_JSON_VALUE)
	private String saveBook(@RequestBody EmpDao empDao) {

		System.out.println(empDao.toString());
		return empServices.saveOrUpdate(empDao);
		
	}

	@PutMapping("/EmpDao")
	private EmpDao update(@RequestBody EmpDao empDao) {
		
		empServices.saveOrUpdate(empDao);
		return empDao;
	}

}
